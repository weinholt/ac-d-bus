;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;;; Serializer and deserializer for the D-Bus wire format

;; Wherein our heroes did not care for ASN.1

(library (d-bus protocol wire)
  (export
    make-d-bus-buffer
    d-bus-buffer-data

    d-bus-put-message
    d-bus-marshal-message!
    d-bus-unmarshal-message)
  (import
    (rnrs)
    (rnrs mutable-pairs)                ;used for mutable boxes
    (d-bus compat match)
    (d-bus protocol signatures)
    (d-bus protocol utils))

#|

  This representation is used for D-Bus values:

  * All basic types have their natural Scheme equivalents.
  * Strings, object paths and signatures are all strings.
  * Structs are represented by lists.
  * Dict entries are (key . value) pairs.
  * Variants are (signature . value).
  * Arrays are vectors.

|#

(define debug #f)

(define (make-d-bus-buffer)
  ;; XXX: It is assumed that the buffer length can always be rounded
  ;; up to a multiple of eight.
  (cons (make-bytevector 64 0) '()))

(define (d-bus-buffer-data buf)
  (car buf))

;; Resize the buffer to hold at least len bytes. Any newly allocated
;; space must be filled with zeros.
(define (enlarge! buf len)
  (when (< (bytevector-length (car buf)) len)
    (let* ((new-len (let lp ((new-len (bytevector-length (car buf))))
                     (if (< new-len len) (lp (* 2 new-len)) new-len)))
           (new-bv (make-bytevector new-len 0)))
      (bytevector-copy! (car buf) 0 new-bv 0 (bytevector-length (car buf)))
      (set-car! buf new-bv)))
  (car buf))

(define (type-alignment type)
  (define (fail x)
    (assertion-violation 'type-alignment "Invalid signature" type))
  (match type
    ['BYTE                     1]
    ['BOOLEAN                  4]
    ['INT16                    2]
    ['UINT16                   2]
    ['INT32                    4]
    [(or 'UINT32 'UNIX_FD)     4]
    ['INT64                    8]
    ['UINT64                   8]
    ['DOUBLE                   8]
    [(or 'STRING 'OBJECT_PATH) 4]
    ['SIGNATURE                1]
    [('STRUCT . type*)         8]
    [('DICT_ENTRY . type*)     8]
    ['VARIANT                  1]
    [('ARRAY atype)            4]
    [_ (fail type)]))

;; Deserialize a message from a bytevector.
(define (d-bus-unmarshal-message bv offset endian sig)
  (define who 'd-bus-unmarshal-message)
  (define (fail x)
    (assertion-violation who "Invalid signature" sig x))
  (define (unmarshal-datum bv offset endian type)
    (when debug
      (write `(unmarshal-datum bv ,(number->string offset 16) ,endian ,type))
      (newline))
    (match type
      ['BYTE
       (values (fx+ offset 1) (bytevector-u8-ref bv offset))]
      ['BOOLEAN
       (let ((offset^ (fxalign offset 4)))
         (values (fx+ offset^ 4)
                 (case (bytevector-u32-native-ref bv offset^)
                   [(0) #f]
                   [(1) #t]
                   [else (fail type)])))]
      ['INT16
       (let ((offset (fxalign offset 2)))
         (values (fx+ offset 2) (bytevector-s16-ref bv offset endian)))]
      ['UINT16
       (let ((offset (fxalign offset 2)))
         (values (fx+ offset 2) (bytevector-u16-ref bv offset endian)))]
      ['INT32
       (let ((offset (fxalign offset 4)))
         (values (fx+ offset 4) (bytevector-s32-ref bv offset endian)))]
      [(or 'UINT32 'UNIX_FD)
       (let ((offset (fxalign offset 4)))
         (values (fx+ offset 4) (bytevector-u32-ref bv offset endian)))]
      ['INT64
       (let ((offset (fxalign offset 8)))
         (values (fx+ offset 8) (bytevector-s64-ref bv offset endian)))]
      ['UINT64
       (let ((offset (fxalign offset 8)))
         (values (fx+ offset 8) (bytevector-u64-ref bv offset endian)))]
      ['DOUBLE
       (let ((offset (fxalign offset 8)))
         (values (fx+ offset 8) (bytevector-ieee-double-ref bv offset endian)))]
      [(or 'STRING 'OBJECT_PATH)
       (let* ((offset (fxalign offset 4))
              (len (bytevector-u32-ref bv offset endian))
              (str (make-bytevector len))
              (offset (fx+ offset 4)))
         (bytevector-copy! bv offset str 0 (bytevector-length str))
         (values (fx+ offset (fx+ len 1)) (utf8->string str)))]

      ['SIGNATURE
       (let* ((len (bytevector-u8-ref bv offset))
              (str (make-bytevector len))
              (offset (fx+ offset 1)))
         (bytevector-copy! bv offset str 0 (bytevector-length str))
         (values (fx+ offset (fx+ len 1)) (utf8->string str)))]

      [('STRUCT . type*)
       (let ((offset (fxalign offset 8)))
         (d-bus-unmarshal-message bv offset endian `(message ,@type*)))]

      [('DICT_ENTRY . type*)
       (let ((offset (fxalign offset 8)))
         (let-values ([(offset vals)
                       (d-bus-unmarshal-message bv offset endian `(message ,@type*))])
           (match vals
             [(k v)
              (values offset `(,k . ,v))])))]

      ['VARIANT
       (let* ((len (bytevector-u8-ref bv offset))
              (str (make-bytevector len)))
         (bytevector-copy! bv (fx+ offset 1) str 0 (bytevector-length str))
         (let ((sig (utf8->string str)))
           (let ((sig^ (parse-type-signature sig)))
             (let ((offset (fx+ offset (fx+ len (fx+ 1 1)))))
               (let-values ([(offset vals)
                             (d-bus-unmarshal-message bv offset endian sig^)])
                 (values offset (cons (cadr sig^) (car vals))))))))]

      [('ARRAY atype)
       (let* ((offset (fxalign offset 4))
              (len (bytevector-u32-ref bv offset endian))
              (offset (fxalign (fx+ offset 4) (type-alignment atype)))
              (end (fx+ offset len))
              (type `(message ,atype)))
         ;; XXX: Should verify that no more than 2^26 bytes is used
         (let lp ((offset offset) (vals '()))
           (cond ((fx=? offset end)
                  (values offset (list->vector (reverse vals))))
                 ((fx>? offset end)
                  (assertion-violation who "Array length incorrect" sig offset end))
                 (else
                  (let-values ([(offset vals^) (d-bus-unmarshal-message bv offset endian type)])
                    (let ((vals (append vals^ vals)))
                      (lp offset vals)))))))]

      [_ (fail type)]))

  (when debug
    (write `(d-bus-unmarshal-message bv ,(number->string offset 16) ,endian ,sig))
    (newline))
  (match sig
    [('message type* ...)
     (let lp ((type* type*) (offset offset) (vals '()))
       (match type*
         [()
          (values offset (reverse vals))]
         [(type . type*^)
          (let-values ([(offset^ val) (unmarshal-datum bv offset endian type)])
            (when debug
              (write (list 'offset offset '=> val))
              (newline))
            (lp type*^ offset^ (cons val vals)))]))]
    [_ (fail sig)]))

;; Serialize a message into a resizable buffer using native
;; endianness. Returns the finishing offset of the data.
(define (d-bus-marshal-message! buf offset sig data*)
  (define who 'd-bus-marshal-message)
  (define (fxalign i alignment)
    (fxand (fx+ i (fx- alignment 1)) (fx- alignment)))
  (define (fail x)
    (assertion-violation who "Invalid signature" sig x))
  ;; Marshal a single datum and return the offset pointing to
  ;; immediately after the data.
  (define (marshal-datum! buf offset type datum)
    (match type
      ;; Basic types
      ['BYTE
       (let* ((end (+ offset 1))
              (bv (enlarge! buf end)))
         (bytevector-u8-set! (car buf) offset datum)
         end)]
      ['BOOLEAN
       (let ((offset (fxalign offset 4)))
         (let* ((end (+ offset 4))
                (bv (enlarge! buf end)))
           (bytevector-u32-native-set! bv offset (if datum 1 0))
           end))]
      ['INT16
       (let ((offset (fxalign offset 2)))
         (let* ((end (+ offset 2))
                (bv (enlarge! buf end)))
           (bytevector-s16-native-set! bv offset datum)
           end))]
      ['UINT16
       (let ((offset (fxalign offset 2)))
         (let* ((end (+ offset 2))
                (bv (enlarge! buf end)))
           (bytevector-u16-native-set! bv offset datum)
           end))]
      ['INT32
       (let ((offset (fxalign offset 4)))
         (let* ((end (+ offset 4))
                (bv (enlarge! buf end)))
           (bytevector-s32-native-set! bv offset datum)
           end))]
      [(or 'UINT32 'UNIX_FD)
       (let ((offset (fxalign offset 4)))
         (let* ((end (+ offset 4))
                (bv (enlarge! buf end)))
           (bytevector-u32-native-set! bv offset datum)
           end))]
      ['INT64
       (let ((offset (fxalign offset 8)))
         (let* ((end (+ offset 8))
                (bv (enlarge! buf end)))
           (bytevector-s64-native-set! bv offset datum)
           end))]
      ['UINT64
       (let ((offset (fxalign offset 8)))
         (let* ((end (+ offset 8))
                (bv (enlarge! buf end)))
           (bytevector-u64-native-set! bv offset datum)
           end))]
      ['DOUBLE
       (let ((offset (fxalign offset 8)))
         (let* ((end (+ offset 8))
                (bv (enlarge! buf end)))
           (bytevector-ieee-double-native-set! bv offset datum)
           (bytevector-u64-native-set! bv offset datum)
           end))]
      [(or 'STRING 'OBJECT_PATH)
       (let ((str (string->utf8 datum))
             (offset (fxalign offset 4)))
         (let* ((end (+ offset (+ (bytevector-length str) 5)))
                (bv (enlarge! buf end)))
           (bytevector-u32-native-set! bv offset (bytevector-length str))
           (bytevector-copy! str 0 bv (+ offset 4) (bytevector-length str))
           end))]
      ['SIGNATURE
       (let ((str (string->utf8 datum)))
         (let* ((end (+ offset (+ (bytevector-length str) 2)))
                (bv (enlarge! buf end)))
           (bytevector-u8-set! bv offset (bytevector-length str))
           (bytevector-copy! str 0 bv (+ offset 1) (bytevector-length str))
           end))]

      ;; Compound types
      [('STRUCT . type*)
       (let ((offset (fxalign offset 8)))
         (d-bus-marshal-message! buf offset `(message ,@type*) datum))]
      [('DICT_ENTRY . type*)
       (match datum
         [(k . v)
          (let ((offset (fxalign offset 8)))
            (d-bus-marshal-message! buf offset `(message ,@type*) (list k v)))]
         [_ (assertion-violation who "Invalid DICT_ENTRY data" sig)])]
      ['VARIANT
       (match datum
         [(signature . value)
          (d-bus-marshal-message! buf offset `(message SIGNATURE ,signature)
                                  (list (format-type-signature `(message ,signature))
                                        value))]
         [_ (assertion-violation who "Invalid VARIANT data" sig)])]
      [('ARRAY atype)
       (let* ((offset (fxalign offset 4))
              (array-length-offset offset))
         (let ((array-start-offset (fxalign (+ offset 4) (type-alignment atype))))
           (let ((type `(message ,atype)))
             (let lp ((i 0) (offset array-start-offset))
               (when (> (- offset array-start-offset) (expt 2 26))
                 (assertion-violation who "Oversized array" sig))
               (cond ((not (= i (vector-length datum)))
                      (lp (fx+ i 1)
                          (d-bus-marshal-message! buf offset type (list (vector-ref datum i)))))
                     (else
                      (let ((bv (enlarge! buf (+ array-start-offset 4))))
                        (bytevector-u32-native-set! bv array-length-offset (- offset array-start-offset))
                        offset)))))))]

      [x (fail x)]))

  (match sig
    [('message type* ...)
     (let lp ((data* data*) (type* type*) (offset offset))
       (match type*
         [()
          (match data*
            [()
             offset]
            [_ (assertion-violation who "Too much data for the signature" sig data*)])]
         [(type . rtype*)
          (match data*
            [() (assertion-violation who "Not enough data for the signature" sig data*)]
            [(datum . rdata*)
             (let ((offset (marshal-datum! buf offset type datum)))
               (lp rdata* rtype* offset))])]))]
    [x (fail x)]))

;; Serialize a message to a port using native endianness
;; Returns the finishing offset of the data.
(define (d-bus-put-message port offset post-align sig data*)
  (define (align-port port offset alignment)
    (let ((padding (fx- (fxalign offset alignment) offset)))
      (put-bytevector port #vu8(0 0 0 0 0 0 0 0) 0 padding)
      (+ offset padding)))
  (unless (memv post-align '(1 2 4 8 16))
    (assertion-violation 'd-bus-put-message "Invalid post-align"
                         port offset post-align sig data*))
  (let* ((buf (make-d-bus-buffer))
         (len (d-bus-marshal-message! buf offset sig data*)))
    (put-bytevector port (car buf) 0 len)
    (align-port port len post-align))))

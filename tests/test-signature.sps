#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

(import
  (rnrs)
  (srfi :64)
  (d-bus protocol signatures))

(test-begin "parse-type-signature")

(test-equal '(message (ARRAY (DICT_ENTRY INT32 INT32)))
            (parse-type-signature "a{ii}"))

(test-equal '(message BYTE BYTE BYTE BYTE UINT32 UINT32 (ARRAY (STRUCT BYTE VARIANT)))
            (parse-type-signature "yyyyuua(yv)"))

(test-equal '(message (STRUCT INT32 (STRUCT INT32 INT32)))
            (parse-type-signature "(i(ii))"))

(test-equal '(message (STRUCT (STRUCT INT32 INT32) INT32))
            (parse-type-signature "((ii)i)"))

(test-equal '(message (ARRAY (ARRAY INT32)))
            (parse-type-signature "aai"))

(test-end)

(test-begin "format-type-signature")

(test-equal "a{ii}i"
            (format-type-signature '(message (ARRAY (DICT_ENTRY INT32 INT32)) INT32)))

(test-equal "ta{oa{sv}}d"
            (format-type-signature
              '(message UINT64
                        (ARRAY (DICT_ENTRY OBJECT_PATH (ARRAY (DICT_ENTRY STRING VARIANT))))
                        DOUBLE)))

(test-equal "yyyyuua(yv)"
            (format-type-signature '(message BYTE BYTE BYTE BYTE UINT32 UINT32
                                             (ARRAY (STRUCT BYTE VARIANT)))))

(define fail-count (test-runner-fail-count (test-runner-get)))
(test-end)

(exit (if (zero? fail-count) 0 1))

;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2019-2020 Göran Weinholt
#!r6rs

;;; Local ("Unix") sockets

(library (d-bus compat socket)
  (export
    local-connect
    socket-textual-ports

    socket-input-port/fds
    socket-can-pass-fds?
    socket-sendmsg

    socket-close

    close-fdes)
  (import
    (rnrs)
    (only (guile) PF_UNIX AF_UNIX SOCK_STREAM
          connect send recv! make-soft-port
          dynamic-func dynamic-link)
    (rename (guile) (socket socket_))
    (only (srfi srfi-1) take)
    (only (system foreign) bytevector->pointer
          pointer->procedure int sizeof))

(define-record-type socket
  (sealed #t)
  (fields (mutable port)
          filename
          ;; File descriptors that have been received on the socket,
          ;; but not yet consumed by the application. These are closed
          ;; when the socket is closed.
          (mutable pending-fds)))

(define (socket-fd sock)
  (fileno (socket-port sock)))

(define lib
  (dynamic-link "libacdbuscompat"))

(define acdbus_recv
  (pointer->procedure int (dynamic-func "acdbus_recv" lib)
                      (list int '* int int '* int)))

(define acdbus_send
  (pointer->procedure int (dynamic-func "acdbus_send" lib)
                      (list int '* int '* int '* int)))

(define (%socket-recv sock bv start count)
  (let* ((maxfds 10)
         (fdbuflen (+ maxfds 1))
         (fdbuf (make-bytevector (* (sizeof int) fdbuflen))))
    (let ((ret (acdbus_recv (socket-fd sock) (bytevector->pointer bv) start count
                            (bytevector->pointer fdbuf) fdbuflen)))
      (when (< ret 0)
        (error '%socket-recv "Receive error" sock ret))
      (let* ((fdlist (bytevector->sint-list fdbuf (native-endianness) (sizeof int)))
             (nfds (car fdlist))
             (fds (take (cdr fdlist) nfds)))
        (socket-pending-fds-set! sock (append fds (socket-pending-fds sock)))
        ret))))

;; Connect to a Unix domain socket (must support abstract sockets!)
(define (local-connect filename)
  (let ((sock (socket_ PF_UNIX SOCK_STREAM 0)))
    (connect sock AF_UNIX filename)
    (make-socket sock filename '())))

(define (socket-input-port sock)
  (define (read! bv start count)
    (%socket-recv sock bv start count))
  (make-custom-binary-input-port "d-bus socket" read! #f #f #f))

;; Get textual ports for working with a connected socket. Must use
;; eol-style crlf!
(define (socket-textual-ports sock)
  (let-values ([(bufp extract) (open-string-output-port)])
    (define (write-char^ c)
      (cond ((eqv? c #\newline)
             (put-string bufp "\r\n"))
            (else
             (put-char bufp c))))
    (define (write-string^ s)
      (string-for-each write-char^ s))
    (define (flush-output^)
      (let ((buf (extract)))
        (send (socket-port sock) (string->utf8 buf))))
    (define (read-char^)
      (let ((bv (make-bytevector 1)))
        (let lp ()
          (let ((n (recv! (socket-port sock) bv)))
            (if (eqv? n 0)
                (eof-object)
                (let ((c (integer->char (bytevector-u8-ref bv 0))))
                  (if (eqv? c #\return)
                      (lp)
                      c)))))))
    (define (close-port^) #f)
    (define buffered-chars^ #f)
    (values (make-soft-port
             (vector #f #f #f
                     read-char^ close-port^ buffered-chars^)
             "r")
            (make-soft-port
             (vector write-char^ write-string^ flush-output^
                     #f close-port^ buffered-chars^)
             "w"))))

;; Fully sends two bytevectors and a list of file descriptors.
(define (socket-sendmsg sock bv0 size0 bv1 size1 fds)
  (let ((ret (acdbus_send (socket-fd sock)
                          (bytevector->pointer bv0) size0
                          (if (bytevector=? #vu8() bv1)
                              (bytevector->pointer #vu8(0)) ;Sagittarius workaround
                              (bytevector->pointer bv1))
                          size1
                          (if (null? fds)
                              (bytevector->pointer #vu8(0))
                              (bytevector->pointer
                               (sint-list->bytevector fds (native-endianness) (sizeof int))))
                          (length fds))))
    (when (< ret 0)
      (error '%socket-send "Send error" sock ret))
    ret))

;; Returns a binary input port for the socket and a procedure that can
;; dequeue fds (or #f if not supported) that have been read since the
;; last dequeuing
(define (socket-input-port/fds sock)
  (values (socket-input-port sock)
          (lambda ()
            (let ((fds (socket-pending-fds sock)))
              (socket-pending-fds-set! sock '())
              fds))))

(define (socket-can-pass-fds? sock)
  #t)

(define (socket-close sock)
  (close-port (socket-port sock))
  (for-each close-fdes (socket-pending-fds sock))
  (socket-port-set! sock #f))

;; Close a received file descriptor
(define (close-fdes fd)
  (@ (guile) close-fdes)))

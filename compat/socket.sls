;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

;;; Unix domain sockets

;; This is a fallback library that uses r6rs-pffi and a small C
;; module. If the Scheme implementation has non-blocking operations
;; then this does not integrate with them at all.

(library (d-bus compat socket)
  (export
    local-connect

    socket-textual-ports

    socket-input-port/fds
    socket-can-pass-fds?
    socket-sendmsg

    socket-close

    close-fdes)
  (import
    (rnrs)
    (pffi))

(define-record-type socket
  (sealed #t)
  (fields (mutable fd)
          filename
          ;; File descriptors that have been received on the socket,
          ;; but not yet consumed by the application. These are closed
          ;; when the socket is closed.
          (mutable pending-fds)))

(define lib
  (open-shared-object "libacdbuscompat.so"))

(define acdbus_connect
  (foreign-procedure lib int acdbus_connect (pointer int)))

(define acdbus_recv
  (foreign-procedure lib int acdbus_recv (int pointer int int pointer int)))

(define acdbus_send
  (foreign-procedure lib int acdbus_send (int pointer int pointer int pointer int)))

(define acdbus_close
  (foreign-procedure lib int acdbus_close (int)))

(define (take lst n)
  (if (= n 0)
      '()
      (cons (car lst) (take (cdr lst) (- n 1)))))

(define (%socket-recv sock bv start count)
  (let* ((maxfds 10)
         (fdbuflen (+ maxfds 1))
         (fdbuf (make-bytevector (* size-of-int fdbuflen))))
    (let ((ret (acdbus_recv (socket-fd sock) (bytevector->pointer bv) start count
                            (bytevector->pointer fdbuf) fdbuflen)))
      (when (< ret 0)
        (error '%socket-recv "Receive error" sock ret))
      (let* ((fdlist (bytevector->sint-list fdbuf (native-endianness) size-of-int))
             (nfds (car fdlist))
             (fds (take (cdr fdlist) nfds)))
        (socket-pending-fds-set! sock (append fds (socket-pending-fds sock)))
        ret))))

;; Connect to a Unix domain socket (must support abstract sockets!)
(define (local-connect filename)
  (let* ((fn (string->utf8 filename))
         (ret (acdbus_connect (bytevector->pointer fn) (bytevector-length fn))))
    (when (< ret 0)
      (error 'local-connect "Could not connect" filename ret))
    (make-socket ret filename '())))

(define (socket-input-port sock)
  (define (read! bv start count)
    (%socket-recv sock bv start count))
  (make-custom-binary-input-port "d-bus socket" read! #f #f #f))

(define (socket-output-port sock)
  (define (write! bv start count)
    (let ((buf (make-bytevector count)))
      (bytevector-copy! bv start buf 0 count)
      (socket-sendmsg sock buf count #vu8() 0 '())))
  (make-custom-binary-output-port "d-bus socket" write! #f #f #f))

;; Get textual ports for working with a connected socket. Must use
;; eol-style crlf!
(define (socket-textual-ports sock)
  (let ((tc (make-transcoder (utf-8-codec) (eol-style crlf))))
    (let ((i (transcoded-port (socket-input-port sock) tc))
          (o (transcoded-port (socket-output-port sock) tc)))
      (values i o))))

;; Fully sends two bytevectors and a list of file descriptors.
(define (socket-sendmsg sock bv0 size0 bv1 size1 fds)
  (let ((ret (acdbus_send (socket-fd sock)
                          (bytevector->pointer bv0) size0
                          (if (bytevector=? #vu8() bv1)
                              (bytevector->pointer #vu8(0)) ;Sagittarius workaround
                              (bytevector->pointer bv1))
                          size1
                          (if (null? fds)
                              (bytevector->pointer #vu8(0))
                              (bytevector->pointer
                               (sint-list->bytevector fds (native-endianness) size-of-int)))
                          (length fds))))
    (when (< ret 0)
      (error '%socket-recv "Send error" sock ret))
    ret))

;; Returns a binary input port for the socket and a procedure that can
;; dequeue fds (or #f if not supported) that have been read since the
;; last dequeuing
(define (socket-input-port/fds sock)
  (values (socket-input-port sock)
          (lambda ()
            (let ((fds (socket-pending-fds sock)))
              (socket-pending-fds-set! sock '())
              fds))))

(define (socket-can-pass-fds? sock)
  #t)

(define (socket-close sock)
  (let ((ret (acdbus_close (socket-fd sock))))
    (when (< ret 0)
      (error 'socket-close "Could not close the socket" sock))
    (for-each acdbus_close (socket-pending-fds sock))
    (socket-fd-set! sock -1)))

;; Close a received file descriptor
(define (close-fdes fd)
  (acdbus_close fd)))

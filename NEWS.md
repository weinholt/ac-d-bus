# Version 1.0.0-beta.0 (2020-05-09)

* Fix array padding for the wire protocol.
* Fix crash on empty signatures in the message header.
* Support for anonymous authentication.
* Support added for Chez Scheme, Sagittarius and Vicare.

# Version 1.0.0-alpha.0 (2020-05-03)

* First release.

#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

;;; Monitor the D-Bus message bus and print it as s-expressions

(import
  (rnrs)
  (d-bus protocol connections)
  (d-bus protocol messages))

(define bus (d-bus-connect))

(d-bus-write-message bus
                     (make-d-bus-message MESSAGE_TYPE_METHOD_CALL 0 #f '()
                                         `#(,(header-PATH        "/org/freedesktop/DBus")
                                            ,(header-DESTINATION "org.freedesktop.DBus")
                                            ,(header-INTERFACE   "org.freedesktop.DBus")
                                            ,(header-MEMBER      "Hello"))
                                         #f))

(d-bus-write-message bus (make-d-bus-message MESSAGE_TYPE_METHOD_CALL 0 #f '()
                                             `#(,(header-PATH        "/org/freedesktop/DBus")
                                                ,(header-DESTINATION "org.freedesktop.DBus")
                                                ,(header-INTERFACE   "org.freedesktop.DBus.Monitoring")
                                                ,(header-MEMBER      "BecomeMonitor")
                                                ,(header-SIGNATURE   "asu"))
                                             '(#() 0)))
(d-bus-conn-flush bus)

(let lp ()
  (let ((msg (d-bus-read-message bus)))
    (write (list 'type (d-bus-message-type msg)
                 'flags (d-bus-message-flags msg)
                 'serial (d-bus-message-serial msg)
                 'fds (d-bus-message-fds msg)
                 'headers (d-bus-headers->alist (d-bus-message-headers msg))
                 'body (d-bus-message-body msg)))
    (newline)
    (flush-output-port (current-output-port))
    (lp)))

(d-bus-disconnect bus)
